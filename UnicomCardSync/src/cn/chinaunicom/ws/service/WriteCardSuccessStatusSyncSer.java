package cn.chinaunicom.ws.service;

import cn.chinaunicom.ws.unibssAttached.UNI_BSS_ATTACHED;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUT;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ;
import cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP;
import cn.chinaunicom.ws.unibssHead.RESPONSE;
import cn.chinaunicom.ws.unibssHead.UNI_BSS_HEAD;

public class WriteCardSuccessStatusSyncSer {
	public WriteCardSuccessStatusSyncSer(){
		
	}
	                                           //  writeCardSuccessStatusSync
	public WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUT writeCardSuccessStatusSync(WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT input){
		WRITE_CARD_SUCCESS_STATUS_SYNC_RSP rsp=new WRITE_CARD_SUCCESS_STATUS_SYNC_RSP();
		WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY body=input.getUNI_BSS_BODY();
		UNI_BSS_HEAD head=input.getUNI_BSS_HEAD();
	
		RESPONSE re=head.getRESPONSE();
		re.setRSP_CODE("00000");
		re.setRSP_DESC("RESPONSE");
		re.setRSP_TYPE("0");
		UNI_BSS_ATTACHED attached=input.getUNI_BSS_ATTACHED();
		WRITE_CARD_SUCCESS_STATUS_SYNC_REQ req=body.getWRITE_CARD_SUCCESS_STATUS_SYNC_REQ();
		
        rsp.setRETURN_DATA_STRING("00000");
      
        WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUT output=new WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUT();
        WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY ob=new WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY();
        ob.setWRITE_CARD_SUCCESS_STATUS_SYNC_RSP(rsp);
        output.setUNI_BSS_ATTACHED(attached);
        output.setUNI_BSS_HEAD(head);
        output.setUNI_BSS_BODY(ob);
		return output;
		
	}

}

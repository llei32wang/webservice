/**
 * UNI_BSS_ATTACHED.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.unibssAttached;

public class UNI_BSS_ATTACHED{
    private java.lang.String MEDIA_INFO;

    public UNI_BSS_ATTACHED() {
    }

    public UNI_BSS_ATTACHED(
           java.lang.String MEDIA_INFO) {
           this.MEDIA_INFO = MEDIA_INFO;
    }


    /**
     * Gets the MEDIA_INFO value for this UNI_BSS_ATTACHED.
     * 
     * @return MEDIA_INFO
     */
    public java.lang.String getMEDIA_INFO() {
        return MEDIA_INFO;
    }


    /**
     * Sets the MEDIA_INFO value for this UNI_BSS_ATTACHED.
     * 
     * @param MEDIA_INFO
     */
    public void setMEDIA_INFO(java.lang.String MEDIA_INFO) {
        this.MEDIA_INFO = MEDIA_INFO;
    }

  

}

/**
 * UNI_BSS_HEADSP_RESERVE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.unibssHead;

public class SP_RESERVE{
    /* 总部流水号 */
    private java.lang.String TRANS_IDC;

    /* 逻辑交易日 */
    private java.lang.String CUTOFFDAY;

    /* 发起方代码 */
    private java.lang.String OSNDUNS;

    /* 归属方代码 */
    private java.lang.String HSNDUNS;

    /* 处理标识 */
    private java.lang.String CONV_ID;

    public SP_RESERVE() {
    }

    public SP_RESERVE(
           java.lang.String TRANS_IDC,
           java.lang.String CUTOFFDAY,
           java.lang.String OSNDUNS,
           java.lang.String HSNDUNS,
           java.lang.String CONV_ID) {
           this.TRANS_IDC = TRANS_IDC;
           this.CUTOFFDAY = CUTOFFDAY;
           this.OSNDUNS = OSNDUNS;
           this.HSNDUNS = HSNDUNS;
           this.CONV_ID = CONV_ID;
    }


    /**
     * Gets the TRANS_IDC value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @return TRANS_IDC   * 总部流水号
     */
    public java.lang.String getTRANS_IDC() {
        return TRANS_IDC;
    }


    /**
     * Sets the TRANS_IDC value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @param TRANS_IDC   * 总部流水号
     */
    public void setTRANS_IDC(java.lang.String TRANS_IDC) {
        this.TRANS_IDC = TRANS_IDC;
    }


    /**
     * Gets the CUTOFFDAY value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @return CUTOFFDAY   * 逻辑交易日
     */
    public java.lang.String getCUTOFFDAY() {
        return CUTOFFDAY;
    }


    /**
     * Sets the CUTOFFDAY value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @param CUTOFFDAY   * 逻辑交易日
     */
    public void setCUTOFFDAY(java.lang.String CUTOFFDAY) {
        this.CUTOFFDAY = CUTOFFDAY;
    }


    /**
     * Gets the OSNDUNS value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @return OSNDUNS   * 发起方代码
     */
    public java.lang.String getOSNDUNS() {
        return OSNDUNS;
    }


    /**
     * Sets the OSNDUNS value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @param OSNDUNS   * 发起方代码
     */
    public void setOSNDUNS(java.lang.String OSNDUNS) {
        this.OSNDUNS = OSNDUNS;
    }


    /**
     * Gets the HSNDUNS value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @return HSNDUNS   * 归属方代码
     */
    public java.lang.String getHSNDUNS() {
        return HSNDUNS;
    }


    /**
     * Sets the HSNDUNS value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @param HSNDUNS   * 归属方代码
     */
    public void setHSNDUNS(java.lang.String HSNDUNS) {
        this.HSNDUNS = HSNDUNS;
    }


    /**
     * Gets the CONV_ID value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @return CONV_ID   * 处理标识
     */
    public java.lang.String getCONV_ID() {
        return CONV_ID;
    }


    /**
     * Sets the CONV_ID value for this UNI_BSS_HEADSP_RESERVE.
     * 
     * @param CONV_ID   * 处理标识
     */
    public void setCONV_ID(java.lang.String CONV_ID) {
        this.CONV_ID = CONV_ID;
    }

}

/**
 * UNI_BSS_HEADCOM_BUS_INFO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.unibssHead;

public class COM_BUS_INFO{
    /* 操作员ID */
    private java.lang.String OPER_ID;

    /* 省分代码 */
    private java.lang.String PROVINCE_CODE;

    /* 地市编码 */
    private java.lang.String EPARCHY_CODE;

    /* 区县编码 */
    private java.lang.String CITY_CODE;

    /* 渠道编码 */
    private java.lang.String CHANNEL_ID;

    /* 渠道类型 */
    private java.lang.String CHANNEL_TYPE;

    /* 接入类型 */
    private java.lang.String ACCESS_TYPE;

    /* 订单提交类型 */
    private java.lang.String ORDER_TYPE;

    public COM_BUS_INFO() {
    }

    public COM_BUS_INFO(
           java.lang.String OPER_ID,
           java.lang.String PROVINCE_CODE,
           java.lang.String EPARCHY_CODE,
           java.lang.String CITY_CODE,
           java.lang.String CHANNEL_ID,
           java.lang.String CHANNEL_TYPE,
           java.lang.String ACCESS_TYPE,
           java.lang.String ORDER_TYPE) {
           this.OPER_ID = OPER_ID;
           this.PROVINCE_CODE = PROVINCE_CODE;
           this.EPARCHY_CODE = EPARCHY_CODE;
           this.CITY_CODE = CITY_CODE;
           this.CHANNEL_ID = CHANNEL_ID;
           this.CHANNEL_TYPE = CHANNEL_TYPE;
           this.ACCESS_TYPE = ACCESS_TYPE;
           this.ORDER_TYPE = ORDER_TYPE;
    }


    /**
     * Gets the OPER_ID value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return OPER_ID   * 操作员ID
     */
    public java.lang.String getOPER_ID() {
        return OPER_ID;
    }


    /**
     * Sets the OPER_ID value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param OPER_ID   * 操作员ID
     */
    public void setOPER_ID(java.lang.String OPER_ID) {
        this.OPER_ID = OPER_ID;
    }


    /**
     * Gets the PROVINCE_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return PROVINCE_CODE   * 省分代码
     */
    public java.lang.String getPROVINCE_CODE() {
        return PROVINCE_CODE;
    }


    /**
     * Sets the PROVINCE_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param PROVINCE_CODE   * 省分代码
     */
    public void setPROVINCE_CODE(java.lang.String PROVINCE_CODE) {
        this.PROVINCE_CODE = PROVINCE_CODE;
    }


    /**
     * Gets the EPARCHY_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return EPARCHY_CODE   * 地市编码
     */
    public java.lang.String getEPARCHY_CODE() {
        return EPARCHY_CODE;
    }


    /**
     * Sets the EPARCHY_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param EPARCHY_CODE   * 地市编码
     */
    public void setEPARCHY_CODE(java.lang.String EPARCHY_CODE) {
        this.EPARCHY_CODE = EPARCHY_CODE;
    }


    /**
     * Gets the CITY_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return CITY_CODE   * 区县编码
     */
    public java.lang.String getCITY_CODE() {
        return CITY_CODE;
    }


    /**
     * Sets the CITY_CODE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param CITY_CODE   * 区县编码
     */
    public void setCITY_CODE(java.lang.String CITY_CODE) {
        this.CITY_CODE = CITY_CODE;
    }


    /**
     * Gets the CHANNEL_ID value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return CHANNEL_ID   * 渠道编码
     */
    public java.lang.String getCHANNEL_ID() {
        return CHANNEL_ID;
    }


    /**
     * Sets the CHANNEL_ID value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param CHANNEL_ID   * 渠道编码
     */
    public void setCHANNEL_ID(java.lang.String CHANNEL_ID) {
        this.CHANNEL_ID = CHANNEL_ID;
    }


    /**
     * Gets the CHANNEL_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return CHANNEL_TYPE   * 渠道类型
     */
    public java.lang.String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }


    /**
     * Sets the CHANNEL_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param CHANNEL_TYPE   * 渠道类型
     */
    public void setCHANNEL_TYPE(java.lang.String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }


    /**
     * Gets the ACCESS_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return ACCESS_TYPE   * 接入类型
     */
    public java.lang.String getACCESS_TYPE() {
        return ACCESS_TYPE;
    }


    /**
     * Sets the ACCESS_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param ACCESS_TYPE   * 接入类型
     */
    public void setACCESS_TYPE(java.lang.String ACCESS_TYPE) {
        this.ACCESS_TYPE = ACCESS_TYPE;
    }


    /**
     * Gets the ORDER_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @return ORDER_TYPE   * 订单提交类型
     */
    public java.lang.String getORDER_TYPE() {
        return ORDER_TYPE;
    }


    /**
     * Sets the ORDER_TYPE value for this UNI_BSS_HEADCOM_BUS_INFO.
     * 
     * @param ORDER_TYPE   * 订单提交类型
     */
    public void setORDER_TYPE(java.lang.String ORDER_TYPE) {
        this.ORDER_TYPE = ORDER_TYPE;
    }

}

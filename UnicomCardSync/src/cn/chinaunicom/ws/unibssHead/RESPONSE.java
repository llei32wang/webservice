/**
 * UNI_BSS_HEADRESPONSE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.unibssHead;

public class RESPONSE{
    /* 应答/错误类型 */
    private java.lang.String RSP_TYPE;

    /* 应答/错误代码 */
    private java.lang.String RSP_CODE;

    /* 应答/错误描述 */
    private java.lang.String RSP_DESC;

    public RESPONSE() {
    }

    public RESPONSE(
           java.lang.String RSP_TYPE,
           java.lang.String RSP_CODE,
           java.lang.String RSP_DESC) {
           this.RSP_TYPE = RSP_TYPE;
           this.RSP_CODE = RSP_CODE;
           this.RSP_DESC = RSP_DESC;
    }


    /**
     * Gets the RSP_TYPE value for this UNI_BSS_HEADRESPONSE.
     * 
     * @return RSP_TYPE   * 应答/错误类型
     */
    public java.lang.String getRSP_TYPE() {
        return RSP_TYPE;
    }


    /**
     * Sets the RSP_TYPE value for this UNI_BSS_HEADRESPONSE.
     * 
     * @param RSP_TYPE   * 应答/错误类型
     */
    public void setRSP_TYPE(java.lang.String RSP_TYPE) {
        this.RSP_TYPE = RSP_TYPE;
    }


    /**
     * Gets the RSP_CODE value for this UNI_BSS_HEADRESPONSE.
     * 
     * @return RSP_CODE   * 应答/错误代码
     */
    public java.lang.String getRSP_CODE() {
        return RSP_CODE;
    }


    /**
     * Sets the RSP_CODE value for this UNI_BSS_HEADRESPONSE.
     * 
     * @param RSP_CODE   * 应答/错误代码
     */
    public void setRSP_CODE(java.lang.String RSP_CODE) {
        this.RSP_CODE = RSP_CODE;
    }


    /**
     * Gets the RSP_DESC value for this UNI_BSS_HEADRESPONSE.
     * 
     * @return RSP_DESC   * 应答/错误描述
     */
    public java.lang.String getRSP_DESC() {
        return RSP_DESC;
    }


    /**
     * Sets the RSP_DESC value for this UNI_BSS_HEADRESPONSE.
     * 
     * @param RSP_DESC   * 应答/错误描述
     */
    public void setRSP_DESC(java.lang.String RSP_DESC) {
        this.RSP_DESC = RSP_DESC;
    }

}

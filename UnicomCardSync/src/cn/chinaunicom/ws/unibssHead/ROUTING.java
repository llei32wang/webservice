/**
 * UNI_BSS_HEADROUTING.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.unibssHead;

public class ROUTING{
    /* 路由类型 */
    private java.lang.String ROUTE_TYPE;

    /* 路由关键值 */
    private java.lang.String ROUTE_VALUE;

    public ROUTING() {
    }

    public ROUTING(
           java.lang.String ROUTE_TYPE,
           java.lang.String ROUTE_VALUE) {
           this.ROUTE_TYPE = ROUTE_TYPE;
           this.ROUTE_VALUE = ROUTE_VALUE;
    }


    /**
     * Gets the ROUTE_TYPE value for this UNI_BSS_HEADROUTING.
     * 
     * @return ROUTE_TYPE   * 路由类型
     */
    public java.lang.String getROUTE_TYPE() {
        return ROUTE_TYPE;
    }


    /**
     * Sets the ROUTE_TYPE value for this UNI_BSS_HEADROUTING.
     * 
     * @param ROUTE_TYPE   * 路由类型
     */
    public void setROUTE_TYPE(java.lang.String ROUTE_TYPE) {
        this.ROUTE_TYPE = ROUTE_TYPE;
    }


    /**
     * Gets the ROUTE_VALUE value for this UNI_BSS_HEADROUTING.
     * 
     * @return ROUTE_VALUE   * 路由关键值
     */
    public java.lang.String getROUTE_VALUE() {
        return ROUTE_VALUE;
    }


    /**
     * Sets the ROUTE_VALUE value for this UNI_BSS_HEADROUTING.
     * 
     * @param ROUTE_VALUE   * 路由关键值
     */
    public void setROUTE_VALUE(java.lang.String ROUTE_VALUE) {
        this.ROUTE_VALUE = ROUTE_VALUE;
    }


}

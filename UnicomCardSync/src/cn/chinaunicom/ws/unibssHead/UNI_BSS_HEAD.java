package cn.chinaunicom.ws.unibssHead;

public class UNI_BSS_HEAD{
    
    private java.lang.String ORIG_DOMAIN;

  
    private java.lang.String SERVICE_NAME;


    private java.lang.String OPERATE_NAME;

  
    private java.lang.String ACTION_CODE;

   
    private java.lang.String ACTION_RELATION;

    private cn.chinaunicom.ws.unibssHead.ROUTING ROUTING;

 
    private java.lang.String PROC_ID;

 
    private java.lang.String TRANS_IDO;

  
    private java.lang.String TRANS_IDH;


    private java.lang.String PROCESS_TIME;

    private cn.chinaunicom.ws.unibssHead.RESPONSE RESPONSE;

    private cn.chinaunicom.ws.unibssHead.COM_BUS_INFO COM_BUS_INFO;

    private cn.chinaunicom.ws.unibssHead.SP_RESERVE SP_RESERVE;

    
    private java.lang.String TEST_FLAG;

  
    private java.lang.String MSG_SENDER;


    private java.lang.String MSG_RECEIVER;

    public UNI_BSS_HEAD() {
    }

    public UNI_BSS_HEAD(
           java.lang.String ORIG_DOMAIN,
           java.lang.String SERVICE_NAME,
           java.lang.String OPERATE_NAME,
           java.lang.String ACTION_CODE,
           java.lang.String ACTION_RELATION,
           cn.chinaunicom.ws.unibssHead.ROUTING ROUTING,
           java.lang.String PROC_ID,
           java.lang.String TRANS_IDO,
           java.lang.String TRANS_IDH,
           java.lang.String PROCESS_TIME,
           cn.chinaunicom.ws.unibssHead.RESPONSE RESPONSE,
           cn.chinaunicom.ws.unibssHead.COM_BUS_INFO COM_BUS_INFO,
           cn.chinaunicom.ws.unibssHead.SP_RESERVE SP_RESERVE,
           java.lang.String TEST_FLAG,
           java.lang.String MSG_SENDER,
           java.lang.String MSG_RECEIVER) {
           this.ORIG_DOMAIN = ORIG_DOMAIN;
           this.SERVICE_NAME = SERVICE_NAME;
           this.OPERATE_NAME = OPERATE_NAME;
           this.ACTION_CODE = ACTION_CODE;
           this.ACTION_RELATION = ACTION_RELATION;
           this.ROUTING = ROUTING;
           this.PROC_ID = PROC_ID;
           this.TRANS_IDO = TRANS_IDO;
           this.TRANS_IDH = TRANS_IDH;
           this.PROCESS_TIME = PROCESS_TIME;
           this.RESPONSE = RESPONSE;
           this.COM_BUS_INFO = COM_BUS_INFO;
           this.SP_RESERVE = SP_RESERVE;
           this.TEST_FLAG = TEST_FLAG;
           this.MSG_SENDER = MSG_SENDER;
           this.MSG_RECEIVER = MSG_RECEIVER;
    }



    public java.lang.String getORIG_DOMAIN() {
        return ORIG_DOMAIN;
    }


    public void setORIG_DOMAIN(java.lang.String ORIG_DOMAIN) {
        this.ORIG_DOMAIN = ORIG_DOMAIN;
    }


   
    public java.lang.String getSERVICE_NAME() {
        return SERVICE_NAME;
    }


  
    public void setSERVICE_NAME(java.lang.String SERVICE_NAME) {
        this.SERVICE_NAME = SERVICE_NAME;
    }


    
    public java.lang.String getOPERATE_NAME() {
        return OPERATE_NAME;
    }


  
    public void setOPERATE_NAME(java.lang.String OPERATE_NAME) {
        this.OPERATE_NAME = OPERATE_NAME;
    }


    public java.lang.String getACTION_CODE() {
        return ACTION_CODE;
    }


    public void setACTION_CODE(java.lang.String ACTION_CODE) {
        this.ACTION_CODE = ACTION_CODE;
    }



    public java.lang.String getACTION_RELATION() {
        return ACTION_RELATION;
    }


    public void setACTION_RELATION(java.lang.String ACTION_RELATION) {
        this.ACTION_RELATION = ACTION_RELATION;
    }


    /**
     * Gets the ROUTING value for this UNI_BSS_HEAD.
     * 
     * @return ROUTING
     */
    public cn.chinaunicom.ws.unibssHead.ROUTING getROUTING() {
        return ROUTING;
    }


  
    public void setROUTING(cn.chinaunicom.ws.unibssHead.ROUTING ROUTING) {
        this.ROUTING = ROUTING;
    }



    public java.lang.String getPROC_ID() {
        return PROC_ID;
    }


  
    public void setPROC_ID(java.lang.String PROC_ID) {
        this.PROC_ID = PROC_ID;
    }


  
    public java.lang.String getTRANS_IDO() {
        return TRANS_IDO;
    }


    public void setTRANS_IDO(java.lang.String TRANS_IDO) {
        this.TRANS_IDO = TRANS_IDO;
    }


    public java.lang.String getTRANS_IDH() {
        return TRANS_IDH;
    }



    public void setTRANS_IDH(java.lang.String TRANS_IDH) {
        this.TRANS_IDH = TRANS_IDH;
    }

    public java.lang.String getPROCESS_TIME() {
        return PROCESS_TIME;
    }


 
    public void setPROCESS_TIME(java.lang.String PROCESS_TIME) {
        this.PROCESS_TIME = PROCESS_TIME;
    }


    /**
     * Gets the RESPONSE value for this UNI_BSS_HEAD.
     * 
     * @return RESPONSE
     */
    public cn.chinaunicom.ws.unibssHead.RESPONSE getRESPONSE() {
        return RESPONSE;
    }


    /**
     * Sets the RESPONSE value for this UNI_BSS_HEAD.
     * 
     * @param RESPONSE
     */
    public void setRESPONSE(cn.chinaunicom.ws.unibssHead.RESPONSE RESPONSE) {
        this.RESPONSE = RESPONSE;
    }



    public cn.chinaunicom.ws.unibssHead.COM_BUS_INFO getCOM_BUS_INFO() {
        return COM_BUS_INFO;
    }


 
    public void setCOM_BUS_INFO(cn.chinaunicom.ws.unibssHead.COM_BUS_INFO COM_BUS_INFO) {
        this.COM_BUS_INFO = COM_BUS_INFO;
    }


 
    public cn.chinaunicom.ws.unibssHead.SP_RESERVE getSP_RESERVE() {
        return SP_RESERVE;
    }


    public void setSP_RESERVE(cn.chinaunicom.ws.unibssHead.SP_RESERVE SP_RESERVE) {
        this.SP_RESERVE = SP_RESERVE;
    }



    public java.lang.String getTEST_FLAG() {
        return TEST_FLAG;
    }


    public void setTEST_FLAG(java.lang.String TEST_FLAG) {
        this.TEST_FLAG = TEST_FLAG;
    }


    public java.lang.String getMSG_SENDER() {
        return MSG_SENDER;
    }



    public void setMSG_SENDER(java.lang.String MSG_SENDER) {
        this.MSG_SENDER = MSG_SENDER;
    }



    public java.lang.String getMSG_RECEIVER() {
        return MSG_RECEIVER;
    }


    public void setMSG_RECEIVER(java.lang.String MSG_RECEIVER) {
        this.MSG_RECEIVER = MSG_RECEIVER;
    }

}

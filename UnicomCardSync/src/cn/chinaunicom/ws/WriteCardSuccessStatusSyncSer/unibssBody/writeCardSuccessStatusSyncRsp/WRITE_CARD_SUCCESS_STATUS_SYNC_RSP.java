/**
 * WRITE_CARD_SUCCESS_STATUS_SYNC_RSP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp;

public class WRITE_CARD_SUCCESS_STATUS_SYNC_RSP{
    private java.lang.String RETURN_DATA_STRING;

    public WRITE_CARD_SUCCESS_STATUS_SYNC_RSP() {
    }

    public WRITE_CARD_SUCCESS_STATUS_SYNC_RSP(
           java.lang.String RETURN_DATA_STRING) {
           this.RETURN_DATA_STRING = RETURN_DATA_STRING;
    }


    /**
     * Gets the RETURN_DATA_STRING value for this WRITE_CARD_SUCCESS_STATUS_SYNC_RSP.
     * 
     * @return RETURN_DATA_STRING
     */
    public java.lang.String getRETURN_DATA_STRING() {
        return RETURN_DATA_STRING;
    }


    /**
     * Sets the RETURN_DATA_STRING value for this WRITE_CARD_SUCCESS_STATUS_SYNC_RSP.
     * 
     * @param RETURN_DATA_STRING
     */
    public void setRETURN_DATA_STRING(java.lang.String RETURN_DATA_STRING) {
        this.RETURN_DATA_STRING = RETURN_DATA_STRING;
    }
}

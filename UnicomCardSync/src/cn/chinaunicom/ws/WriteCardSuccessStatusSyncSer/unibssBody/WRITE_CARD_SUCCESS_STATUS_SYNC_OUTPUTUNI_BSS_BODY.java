/**
 * WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody;

public class WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY{
    private cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP WRITE_CARD_SUCCESS_STATUS_SYNC_RSP;

    public WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY() {
    }

    public WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY(
           cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP WRITE_CARD_SUCCESS_STATUS_SYNC_RSP) {
           this.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP = WRITE_CARD_SUCCESS_STATUS_SYNC_RSP;
    }


    /**
     * Gets the WRITE_CARD_SUCCESS_STATUS_SYNC_RSP value for this WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY.
     * 
     * @return WRITE_CARD_SUCCESS_STATUS_SYNC_RSP
     */
    public cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP getWRITE_CARD_SUCCESS_STATUS_SYNC_RSP() {
        return WRITE_CARD_SUCCESS_STATUS_SYNC_RSP;
    }


    /**
     * Sets the WRITE_CARD_SUCCESS_STATUS_SYNC_RSP value for this WRITE_CARD_SUCCESS_STATUS_SYNC_OUTPUTUNI_BSS_BODY.
     * 
     * @param WRITE_CARD_SUCCESS_STATUS_SYNC_RSP
     */
    public void setWRITE_CARD_SUCCESS_STATUS_SYNC_RSP(cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncRsp.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP WRITE_CARD_SUCCESS_STATUS_SYNC_RSP) {
        this.WRITE_CARD_SUCCESS_STATUS_SYNC_RSP = WRITE_CARD_SUCCESS_STATUS_SYNC_RSP;
    }

}

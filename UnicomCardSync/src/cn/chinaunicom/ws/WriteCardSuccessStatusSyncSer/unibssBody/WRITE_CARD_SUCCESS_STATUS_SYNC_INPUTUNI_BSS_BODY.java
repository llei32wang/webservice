/**
 * WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody;

public class WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY{
    private cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ WRITE_CARD_SUCCESS_STATUS_SYNC_REQ;

    public WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY() {
    }

    public WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY(
           cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ WRITE_CARD_SUCCESS_STATUS_SYNC_REQ) {
           this.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ = WRITE_CARD_SUCCESS_STATUS_SYNC_REQ;
    }


    /**
     * Gets the WRITE_CARD_SUCCESS_STATUS_SYNC_REQ value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY.
     * 
     * @return WRITE_CARD_SUCCESS_STATUS_SYNC_REQ
     */
    public cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ getWRITE_CARD_SUCCESS_STATUS_SYNC_REQ() {
        return WRITE_CARD_SUCCESS_STATUS_SYNC_REQ;
    }


    /**
     * Sets the WRITE_CARD_SUCCESS_STATUS_SYNC_REQ value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY.
     * 
     * @param WRITE_CARD_SUCCESS_STATUS_SYNC_REQ
     */
    public void setWRITE_CARD_SUCCESS_STATUS_SYNC_REQ(cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ WRITE_CARD_SUCCESS_STATUS_SYNC_REQ) {
        this.WRITE_CARD_SUCCESS_STATUS_SYNC_REQ = WRITE_CARD_SUCCESS_STATUS_SYNC_REQ;
    }

}

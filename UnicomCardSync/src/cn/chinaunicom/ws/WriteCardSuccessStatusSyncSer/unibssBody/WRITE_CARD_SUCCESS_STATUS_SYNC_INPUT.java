/**
 * WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody;

public class WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT{
    private cn.chinaunicom.ws.unibssHead.UNI_BSS_HEAD UNI_BSS_HEAD;

    private cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY UNI_BSS_BODY;

    private cn.chinaunicom.ws.unibssAttached.UNI_BSS_ATTACHED UNI_BSS_ATTACHED;

    public WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT() {
    }

    public WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT(
           cn.chinaunicom.ws.unibssHead.UNI_BSS_HEAD UNI_BSS_HEAD,
           cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY UNI_BSS_BODY,
           cn.chinaunicom.ws.unibssAttached.UNI_BSS_ATTACHED UNI_BSS_ATTACHED) {
           this.UNI_BSS_HEAD = UNI_BSS_HEAD;
           this.UNI_BSS_BODY = UNI_BSS_BODY;
           this.UNI_BSS_ATTACHED = UNI_BSS_ATTACHED;
    }


    /**
     * Gets the UNI_BSS_HEAD value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @return UNI_BSS_HEAD
     */
    public cn.chinaunicom.ws.unibssHead.UNI_BSS_HEAD getUNI_BSS_HEAD() {
        return UNI_BSS_HEAD;
    }


    /**
     * Sets the UNI_BSS_HEAD value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @param UNI_BSS_HEAD
     */
    public void setUNI_BSS_HEAD(cn.chinaunicom.ws.unibssHead.UNI_BSS_HEAD UNI_BSS_HEAD) {
        this.UNI_BSS_HEAD = UNI_BSS_HEAD;
    }


    /**
     * Gets the UNI_BSS_BODY value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @return UNI_BSS_BODY
     */
    public cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY getUNI_BSS_BODY() {
        return UNI_BSS_BODY;
    }


    /**
     * Sets the UNI_BSS_BODY value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @param UNI_BSS_BODY
     */
    public void setUNI_BSS_BODY(cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.WRITE_CARD_SUCCESS_STATUS_SYNC_INPUTUNI_BSS_BODY UNI_BSS_BODY) {
        this.UNI_BSS_BODY = UNI_BSS_BODY;
    }


    /**
     * Gets the UNI_BSS_ATTACHED value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @return UNI_BSS_ATTACHED
     */
    public cn.chinaunicom.ws.unibssAttached.UNI_BSS_ATTACHED getUNI_BSS_ATTACHED() {
        return UNI_BSS_ATTACHED;
    }


    /**
     * Sets the UNI_BSS_ATTACHED value for this WRITE_CARD_SUCCESS_STATUS_SYNC_INPUT.
     * 
     * @param UNI_BSS_ATTACHED
     */
    public void setUNI_BSS_ATTACHED(cn.chinaunicom.ws.unibssAttached.UNI_BSS_ATTACHED UNI_BSS_ATTACHED) {
        this.UNI_BSS_ATTACHED = UNI_BSS_ATTACHED;
    }

}

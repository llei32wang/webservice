/**
 * WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.chinaunicom.ws.WriteCardSuccessStatusSyncSer.unibssBody.writeCardSuccessStatusSyncReq;

public class WRITE_CARD_SUCCESS_STATUS_SYNC_REQ{
    private java.lang.String BUSINESS_NUM;

    private java.lang.String ICC_ID_NUM;

    private java.lang.String PRE_SUBMIT_PROVINCE_ID;

    private java.lang.String WRITE_CARD_SUCCESS_STATUS;

    public WRITE_CARD_SUCCESS_STATUS_SYNC_REQ() {
    }

    public WRITE_CARD_SUCCESS_STATUS_SYNC_REQ(
           java.lang.String BUSINESS_NUM,
           java.lang.String ICC_ID_NUM,
           java.lang.String PRE_SUBMIT_PROVINCE_ID,
           java.lang.String WRITE_CARD_SUCCESS_STATUS) {
           this.BUSINESS_NUM = BUSINESS_NUM;
           this.ICC_ID_NUM = ICC_ID_NUM;
           this.PRE_SUBMIT_PROVINCE_ID = PRE_SUBMIT_PROVINCE_ID;
           this.WRITE_CARD_SUCCESS_STATUS = WRITE_CARD_SUCCESS_STATUS;
    }


    /**
     * Gets the BUSINESS_NUM value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @return BUSINESS_NUM
     */
    public java.lang.String getBUSINESS_NUM() {
        return BUSINESS_NUM;
    }


    /**
     * Sets the BUSINESS_NUM value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @param BUSINESS_NUM
     */
    public void setBUSINESS_NUM(java.lang.String BUSINESS_NUM) {
        this.BUSINESS_NUM = BUSINESS_NUM;
    }


    /**
     * Gets the ICC_ID_NUM value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @return ICC_ID_NUM
     */
    public java.lang.String getICC_ID_NUM() {
        return ICC_ID_NUM;
    }


    /**
     * Sets the ICC_ID_NUM value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @param ICC_ID_NUM
     */
    public void setICC_ID_NUM(java.lang.String ICC_ID_NUM) {
        this.ICC_ID_NUM = ICC_ID_NUM;
    }


    /**
     * Gets the PRE_SUBMIT_PROVINCE_ID value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @return PRE_SUBMIT_PROVINCE_ID
     */
    public java.lang.String getPRE_SUBMIT_PROVINCE_ID() {
        return PRE_SUBMIT_PROVINCE_ID;
    }


    /**
     * Sets the PRE_SUBMIT_PROVINCE_ID value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @param PRE_SUBMIT_PROVINCE_ID
     */
    public void setPRE_SUBMIT_PROVINCE_ID(java.lang.String PRE_SUBMIT_PROVINCE_ID) {
        this.PRE_SUBMIT_PROVINCE_ID = PRE_SUBMIT_PROVINCE_ID;
    }


    /**
     * Gets the WRITE_CARD_SUCCESS_STATUS value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @return WRITE_CARD_SUCCESS_STATUS
     */
    public java.lang.String getWRITE_CARD_SUCCESS_STATUS() {
        return WRITE_CARD_SUCCESS_STATUS;
    }


    /**
     * Sets the WRITE_CARD_SUCCESS_STATUS value for this WRITE_CARD_SUCCESS_STATUS_SYNC_REQ.
     * 
     * @param WRITE_CARD_SUCCESS_STATUS
     */
    public void setWRITE_CARD_SUCCESS_STATUS(java.lang.String WRITE_CARD_SUCCESS_STATUS) {
        this.WRITE_CARD_SUCCESS_STATUS = WRITE_CARD_SUCCESS_STATUS;
    }

}

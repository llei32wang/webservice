package cn.txez.ws;

public class WRITE_CARD_STATUS_SYNC_REQ {
    String BUSINESS_NUM;
    String ICC_ID_NUM;
    String PRE_SUBMIT_PROVINCE_ID;
    String WRITE_CARD_STATUS;
	public String getBUSINESS_NUM() {
		return BUSINESS_NUM;
	}
	public void setBUSINESS_NUM(String bUSINESS_NUM) {
		BUSINESS_NUM = bUSINESS_NUM;
	}
	public String getICC_ID_NUM() {
		return ICC_ID_NUM;
	}
	public void setICC_ID_NUM(String iCC_ID_NUM) {
		ICC_ID_NUM = iCC_ID_NUM;
	}
	public String getPRE_SUBMIT_PROVINCE_ID() {
		return PRE_SUBMIT_PROVINCE_ID;
	}
	public void setPRE_SUBMIT_PROVINCE_ID(String pRE_SUBMIT_PROVINCE_ID) {
		PRE_SUBMIT_PROVINCE_ID = pRE_SUBMIT_PROVINCE_ID;
	}
	public String getWRITE_CARD_STATUS() {
		return WRITE_CARD_STATUS;
	}
	public void setWRITE_CARD_STATUS(String wRITE_CARD_STATUS) {
		WRITE_CARD_STATUS = wRITE_CARD_STATUS;
	}
    
}
